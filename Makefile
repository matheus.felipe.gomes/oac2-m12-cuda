PROG_NAME = gpu_printf

build:
	nvcc ${PROG_NAME}.cu -o ${PROG_NAME}
	nvcc -arch compute_20 ${PROG_NAME}.cu -o ${PROG_NAME}_arch

run:
	./${PROG_NAME} > ${PROG_NAME}.txt
	./${PROG_NAME}_arch > ${PROG_NAME}_arch.txt

buildrun: build run