#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define X_DIM_MAX 3
#define Y_DIM_MAX 3
#define Z_DIM_MAX 2

__global__ void print_gpu_idx()
{
    printf("---\n");
    printf("gridDim.x: %d; gridDim.y: %d;\n", gridDim.x, gridDim.y);
    printf("blockDim.x: %d; blockDim.y: %d; blockDim.z: %d;\n", blockDim.x, blockDim.y, blockDim.z);
    printf("blockIdx.x: %d; blockIdx.y: %d; blockIdx.z: %d;\n", blockIdx.x, blockIdx.y, blockIdx.z);
    printf("threadIdx.x: %d; threadIdx.y: %d; threadIdx.z: %d;\n", threadIdx.x, threadIdx.y, threadIdx.z);
    printf("---\n");
}

int main()
{
    int dimx, dimy, dimz;
    dim3 grid, blocks;

    for(int i = 0; i < X_DIM_MAX; i++)
    {
        for(int j = 0; j < Y_DIM_MAX; j++)
        {
            for(int k = 0; k < Z_DIM_MAX; k++)
            {
                dimx = i+1; dimy = j+1; dimz = k+1;
                grid.x = dimx; blocks.x = dimx;
                grid.y = dimy; blocks.y = dimy;
                blocks.z = dimz;
                printf("lauching kernel print_gpu_idx with: grid(%d, %d, %d) and blocks(%d, %d, %d);\n", dimx, dimy, dimz, dimx, dimy, dimz);
                print_gpu_idx<<<grid, blocks>>>();
                cudaDeviceSynchronize();
            } 
        }
    }
}