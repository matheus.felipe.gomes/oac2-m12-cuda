**Escola Politécnica da Universidade de São Paulo**

**Disciplina**: PCS3422 - Organização e Arquitetura de Computadores

**Nome**: Matheus Felipe Gomes

**NUSP**: 8993198

**Data**: 15 de Novembro de 2020

**GitLab**: [@matheus.felipe.gomes](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes)

**Repositório GitLab**: [oac2-m12-cuda](https://gitlab.uspdigital.usp.br/matheus.felipe.gomes/oac2-m12-cuda)

**Professor**: Dr. Bruno Albertini

**Título**: Módulo 12 - CUDA

### 2. Experiment with printf() inside the kernel. Try printing out the values of threadIdx.x and blockIdx.x for some or all of the threads. 

*Do they print in sequential order?*

R: Geralmente, não há uma ordem determinística da execução dos printf pelos threads.


*Why or why not?*

R: Os blocos de threads permitem escalabilidade. A execução dos blocos pode ocorrer em qualquer ordem, e é justamente essa independência entre blocos que possibilita a escalabilidade, pois um kernel pode escalar através de múltiplos SMs (Streaming Multiprocessor).


### 3. Print the value of threadIdx.y or threadIdx.z (or blockIdx.y) in the kernel. (Likewise for blockDim and gridDim). 

Ver anexo.

*Why do these exist?*

R: Uma das abstrações chaves do modelo CUDA é a hierarquia de grupos de thread. Além disso, grande parte dos problemas alvos consiste de processamento gráfico em 3 dimensões ou que pode ser mapeado a partir de um espaço tridimensional. Portanto, a razão da existência de tais variáveis é a facilitar a decomposição de problemas em um espaço tridimensional.


*How do you get them to take on values other than 0 (1 for the dims)?* 

R: Basta modificar os parâmetros de execução dos kernels utilizando `dim3` com campos diferente de 1 (valor default). Por exemplo, utilizando `kernel<<<dim3 grid(2,3), dim3 blocks(2,3,4)>>>(...)`:
- kernel é executado em uma grid contendo 2x2=4 blocos indexados por (blockIdx.x, blockIdx.y), com
    - blockIdx.x em {0, 1}
    - blockIdx.y em {0, 1, 2};
- cada bloco da grid contendo 2x3x4=24 threads indexados por (threadIdx.x, threadIdx.y, threadIdx.z), com 
    - threadIdx.x em {0, 1};
    - threadIdx.y em {0, 1, 2};
    - threadIdx.z em {0, 1, 2, 3};


### Anexo

```
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>

#define X_DIM_MAX 3
#define Y_DIM_MAX 3
#define Z_DIM_MAX 2

__global__ void print_gpu_idx()
{
    printf("gridDim.x: %d; gridDim.y: %d; gridDim.z: %d;\n", gridDim.x, gridDim.y, gridDim.z);
    printf("blockDim.x: %d; blockDim.y: %d; blockDim.z: %d;\n", blockDim.x, blockDim.y, blockDim.z);
    printf("blockIdx.x: %d; blockIdx.y: %d; blockIdx.z: %d;\n", blockIdx.x, blockIdx.y, blockIdx.z);
    printf("threadIdx.x: %d; threadIdx.y: %d; threadIdx.z: %d;\n", threadIdx.x, threadIdx.y, threadIdx.z);
}

int main()
{
    int dimx, dimy, dimz;
    for(int i = 0; i < X_DIM_MAX; i++)
    {
        for(int j = 0; j < Y_DIM_MAX; j++)
        {
            for(int k = 0; k < Z_DIM_MAX; k++)
            {
                dimx = i+1; dimy = j+1; dimz = k+1;
                dim3 grid(dimx, dimy, dimz);
                dim3 blocs(dimx, dimy, dimz);
                printf("lauching kernel print_gpu_idx with: grid(%d, %d, %d) and blocs(%d, %d, %d);\n", dimx, dimy, dimz, dimx, dimy, dimz);
                print_gpu_idx<<<grid, blocs>>>();
            } 
        }
    }
}
```


```
...

lauching kernel print_gpu_idx with: grid(2, 3, 1) and blocks(2, 3, 1);
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
gridDim.x: 2; gridDim.y: 3;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockDim.x: 2; blockDim.y: 3; blockDim.z: 1;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 2; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 1; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 1; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
blockIdx.x: 0; blockIdx.y: 0; blockIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 0; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 1; threadIdx.z: 0;
threadIdx.x: 0; threadIdx.y: 2; threadIdx.z: 0;
threadIdx.x: 1; threadIdx.y: 2; threadIdx.z: 0;
...
```